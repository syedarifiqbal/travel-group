<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArrivalHourAndFlightNoFieldOnCustomerGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_group', function (Blueprint $table) {
            $table->string('flight_no')->nullable();
            $table->string('arrival_hour')->nullable();
        });
        Schema::table('groups', function (Blueprint $table) {
            $table->string('arrival_hour')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_group', function (Blueprint $table) {
            $table->dropColumn(['flight_no', 'arrival_hour']);
        });
        Schema::table('groups', function (Blueprint $table) {
            $table->dropColumn('arrival_hour');
        });
    }
}
