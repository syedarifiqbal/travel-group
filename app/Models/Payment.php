<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'amount', 'customer_id', 'created_by', 'proof', 'payment_method', 'remarks'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class, '');
    }

    public function customer()
    {
        return $this->belongsTo(User::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
