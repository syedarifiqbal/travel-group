<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
    protected $fillable = [ 'group_id', 'customer_id', 'arrival_hour', 'flight_no' ];

    protected $table = 'customer_group';

    public $timestamps = false;

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function customers()
    {
        return $this->belongsTo(User::class);
    }

}
