<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'destination_id',
        'arrival_date',
        'arrival_hour',
        'out_date',
        'special_requirements',
        'active',
        'created_by'
    ];

    protected $dates = [
        'arrival_date',
        'out_date'
    ];

    public function setArrivalDateAttribute($value)
    {
        $s = explode('/', $value);
        $this->attributes['arrival_date'] = now()->setDate($s[2],$s[1],$s[0]);
    }

    public function setOutDateAttribute($value)
    {
        $s = explode('/', $value);
        $this->attributes['out_date'] = now()->setDate($s[2],$s[1],$s[0]);
    }

    public function customers()
    {
        return $this->belongsToMany(User::class, 'customer_group', 'group_id', 'customer_id')
            ->withPivot('price', 'flight_no', 'arrival_hour');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function destination()
    {
        return $this->belongsTo(Destination::class);
    }

}
