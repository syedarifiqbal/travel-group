<?php

namespace App;

use App\Models\CustomerGroup;
use App\Models\Destination;
use App\Models\Group;
use App\Models\Payment;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'age', 'city', 'state', 'phone', 'email', 'cpf', 'rg', 'password', 'created_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function destinations()
    {
        return $this->hasMany(Destination::class, 'created_by');
    }

    public function groups()
    {
        return $this->hasMany(Group::class, 'created_by');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'customer_id');
    }

    public function CustomerGroup()
    {
        return $this->belongsToMany(User::class, 'customer_group', 'group_id', 'customer_id')->withPivot('price');
    }

    /**
     * Get all of the customers for groups.
     */
    public function customer_payments()
    {
        return $this->hasMany(Payment::class, 'created_by');
    }

    /**
     * Get all of the customers for groups.
     */
    public function customers()
    {
        return $this->hasMany(User::class, 'created_by');
    }

    /**
     * check if user is manager.
     */
    public function isManager()
    {
        return $this->type == 'manager';
    }

    /**
     * check if user is manager.
     */
    public function deactivate()
    {
        $this->active = !$this->active;
        $this->save();
        return $this;
    }
}
