<?php

namespace App\Http\Middleware;

use Closure;

class ChangeLocaleBasedOnMe
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $englishEmails = ['arifiqbal@outlook.com', 'hdarif2@gmail.com', 'admin@admin.com'];
        if(auth()->user() && in_array( auth()->user()->email, $englishEmails ))
        {
            config(['app.locale' => 'en']);
        }
        return $next($request);
    }
}
