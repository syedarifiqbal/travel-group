<?php

namespace App\Http\Middleware;

use Closure;

class AdminOrManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {

        $roles = array_slice(func_get_args(), 2); // [default, admin, manager]

        if (auth()->guard('admin')->check()){
            return $next($request);
        }

        if (auth()->guard('web')->check() && auth()->user()->isManager()){
            return $next($request);
        }

        /*foreach ($roles as $role) {

            try {

                Role::whereName($role)->firstOrFail(); // make sure we got a "real" role

                if (Auth::user()->hasRole($role)) {
                    return $next($request);
                }

            } catch (ModelNotFoundException $exception) {

                dd('Could not find role ' . $role);

            }
        }

        Flash::warning('Access Denied', 'You are not authorized to view that content.'); // custom flash class

        return redirect('/');*/
        return redirect()->route('login');
    }
}
