<?php

namespace App\Http\Middleware;

use Closure;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /*if ($request->user()->type === 'admin'){
            return $next($request);
        }*/

        if (auth()->guard('admin')->check()){
            return $next($request);
        }
        return redirect()->route('admin.dashboard')->with('error','You have not admin access');
    }
}
