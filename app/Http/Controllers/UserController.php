<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $customers = User::where('created_by', auth()->id())->get();
        return view('users.index', compact('customers'));
    }

    public function userJson()
    {
        $customers = User::where('created_by', auth()->id())
            ->where('type', 'default')->get();
        return $customers;
    }

    public function create()
    {
        return view('auth.register', ['createdBy' => true]);
    }

    public function store(Request $request)
    {

        $data = $request->validate([
            'first_name'    => ['required', 'string', 'max:255'],
            'last_name'     => ['required', 'string', 'max:255'],
            'phone'         => ['required', 'string', 'max:15'],
            'age'           => ['required', 'date_format:d/m/Y'],
            'city'          => ['required', 'string', 'max:255'],
            'state'         => ['required', 'string', 'max:255'],
            'cpf'           => ['required', 'string', 'max:255'],
            'rg'            => ['required', 'string', 'max:255'],
            'email'         => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'      => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'city' => $data['city'],
            'state' => $data['state'],
            'age' => $data['age'],
            'email' => $data['email'],
            'cpf' => $data['cpf'],
            'rg' => $data['rg'],
            'password' => Hash::make($data['password']),
            'created_by' => auth()->id(),
        ]);

        return redirect()->route('home')->with('message', 'User Created Successfully');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer()
    {
//        return User::select('id', 'first_name')->where('id', '=', auth()->user()->id)->get();
        return User::select('id', 'first_name')->where('id', '!=', auth()->id())->get();
    }
}
