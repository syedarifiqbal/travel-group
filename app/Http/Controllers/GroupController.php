<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = auth()->user()->groups()->withCount('customers')->with('destination')->get();
        return view('groups.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('groups.create', ['group' => new Group()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'destination_id' => 'required|exists:destinations,id',
            'arrival_date' => 'required|date_format:d/m/Y',
            'out_date' => 'required|date_format:d/m/Y',
            'customers' => 'required|array',
            // 'special_requirements' => 'required',
            'customers.*.name.id' => 'required',
            'customers.*.price' => 'required',
        ]);

        $syncIds = [];
        foreach ($request->customers as $customer)
        {
            $syncIds[$customer['name']['id']] = [ 'price' => $customer['price'] ];
        }

        $group = new Group($request->all());
        auth()->user()->groups()->save($group);
        $group->customers()->sync($syncIds);

        return response()->make(json_encode([
            'status' => 'success',
            'message' => 'Group inserted successfully!',
            'returnUrl' => route('groups.index')
        ]), 201);

        // return redirect()->route('destinations.index')->with('message', 'Destinations Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Request $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Group $group)
    {
        if ($request->isXmlHttpRequest())
        {
            $group->load('destination');
            return $group;
        }
        return view('groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return @any
     */
    public function edit(Request $request, Group $group)
    {
        if ($request->isXmlHttpRequest()){
            $group->load('customers');
            return $group;
        }
        return view('groups.create', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $request->validate([
            'destination_id' => 'required|exists:destinations,id',
            'arrival_date' => 'required|date_format:d/m/Y',
            'out_date' => 'required|date_format:d/m/Y',
            'arrival_hour' => 'required',
            // 'special_requirements' => 'required',
            'customers' => 'required|array',
            'customers.*.name.id' => 'required',
            'customers.*.price' => 'required',
        ]);

        $syncIds = [];
        foreach ($request->customers as $customer)
        {
            $syncIds[$customer['name']['id']] = [ 'price' => $customer['price'] ];
        }

        $group->fill($request->all());
        $group->save();
        $group->customers()->sync($syncIds);

        return response()->make(json_encode([
            'status' => 'success',
            'message' => 'Group updated successfully!',
            'returnUrl' => route('groups.index')
        ]), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Group $group)
    {
        $group->delete();
        return redirect()->route('groups.index')->with('message', 'Group Deleted Successfully');
    }

    /**
     * Return customers for given group.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function customerJson(Group $group)
    {
        return $group->customers;
    }


}
