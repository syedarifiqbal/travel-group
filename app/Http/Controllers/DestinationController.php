<?php

namespace App\Http\Controllers;

use App\Models\Destination;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if(auth()->guard('admin')->check())
        {
            $user = auth()->guard('admin')->user();
        }
        if (request()->wantsJson())
        {
            /*return Destination::select('id', 'name')->get();*/
            return $user->destinations;
        }
        return view('destinations.index', ['records' => $user->destinations()->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('destinations.create', ['destination' => new Destination()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'max_capacity' => 'required|integer',
            'city' => 'required|string',
            'price' => 'required|integer'
        ]);

        $destination = new Destination($request->all());
        auth()->user()->destinations()->save($destination);

        return redirect()->route('destinations.index')->with('message', 'Destinations Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function show(Destination $destination)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function edit(Destination $destination)
    {
        return view('destinations.create', ['destination' => $destination]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Destination $destination)
    {
        $request->validate([
            'name' => 'required',
            'max_capacity' => 'required|integer',
            'city' => 'required|string',
            'price' => 'required|integer'
        ]);

        $destination->fill($request->all())->save();

        return redirect()->route('destinations.index')->with('message', 'Destinations Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Destination $destination
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Destination $destination)
    {
        if($destination->groups()->exists())
        {
            return redirect()->route('destinations.index')->with('error', 'Destination has one or more Groups related please delete group first.');
        }

        $destination->delete();
        return redirect()->route('destinations.index')->with('message', 'Destinations Deleted Successfully');
    }
}
