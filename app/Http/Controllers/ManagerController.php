<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Admin;
class ManagerController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:manager',['only' => 'index','edit']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.managers.index', ['records' => User::withTrashed()->where('type', 'manager')->get()]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manager = new User(['type' => 'manager']);
        return view('admin.managers.create', compact('manager'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $request->validate([
            'name'          => 'required',
            'email'         => 'required',
            'password'      => 'required'
        ]);
        // store in the database
        $admins = new Admin;
        $admins->name = $request->name;
        $admins->email = $request->email;
        $admins->password=bcrypt($request->password);
        $admins->save();
        return redirect()->route('manager.auth.login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function managerStore(Request $request)
    {
        // validate the data
        $request->validate([
            'name'          => 'required',
            'email'         => 'required|email|unique:users',
            'password'      => 'required'
        ]);
        // store in the database
        $admins = new User;
        $admins->first_name = $request->name;
        $admins->last_name = '';
        $admins->email = $request->email;
        $admins->created_by = auth()->guard('admin')->id();
        $admins->type = 'manager';
        $admins->phone = '';
        $admins->age = '';
        $admins->city = '';
        $admins->state = '';
        $admins->password=bcrypt($request->password);
        $admins->save();
        return redirect()->route('admin.dashboard');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $manager)
    {
        return view('admin.managers.create', compact('manager'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $manager)
    {
        // validate the data
        $request->validate([
            'name'          => 'required',
            'email'         => 'required|email|unique:users,email,'.$manager->id,
        ]);
        // store in the database
        $manager->first_name = $request->name;
        $manager->email = $request->email;
        // return $manager->first_name;
        $manager->save();
        return redirect()->route('managers.index');
    }
    /**
     * Deactivate Manager.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(User $manager)
    {
        $manager->deactivate();
        return response(['message' => 'status change']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $manager)
    {
        $manager->delete();
        return redirect()->route('managers.index');
    }
}