<?php

namespace App\Http\Controllers;

use App\Models\CustomerGroup;
use App\Models\Destination;
use App\Models\Group;
use App\Models\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SeedsController extends Controller
{
    function index()
    {
        if(Gate::denies('can-seed-data'))
            return redirect('/');
//        return User::all();
//        $this->userSeed();
//        $this->destinationSeed();
//        $this->groupsSeed();
//        $this->customerGroupsSeed();
//        $this->paymentsSeed();
//        $this->InsertUsers();
//        $this->InsertDestinations();
//        $this->InsertGroups();
//        $this->InsertCustomersGroup();
//        $this->InsertPayments();
    }

    protected function userSeed()
    {
        $users = User::all();
        echo '<pre>';
        foreach($users as $user)
        {
            $datas = $user->toArray();
            echo "\$user = new \App\User();\r\n";
            foreach($datas as $column => $value)
            {
                if(!is_null($value))
                    echo "\$user->{$column} = '$value';\r\n";
            }
            echo "\$user->save();\r\n\r\n";
        }
        echo '</pre>';
    }

    protected function destinationSeed()
    {
        $destinations = Destination::all();
        echo '<pre>';
        foreach($destinations as $destination)
        {
            $datas = $destination->toArray();
            echo "\$destination = new \App\Models\Destination();\r\n";
            foreach($datas as $column => $value)
            {
                if(!is_null($value))
                    echo "\$destination->{$column} = '$value';\r\n";
            }
            echo "\$destination->save();\r\n\r\n";
        }
        echo '</pre>';
    }

    protected function groupsSeed()
    {
        $groups = Group::all();
        echo '<pre>';
        foreach($groups as $group)
        {
            $datas = $group->toArray();
            echo "\$group = new \App\Models\Group();\r\n";
            foreach($datas as $column => $value)
            {
                if(!is_null($value))
                    echo "\$group->{$column} = '$value';\r\n";
            }
            echo "\$group->save();\r\n\r\n";
        }
        echo '</pre>';
    }

    protected function customerGroupsSeed()
    {
        $groups = CustomerGroup::all();
        echo '<pre>';
        foreach($groups as $group)
        {
            $datas = $group->toArray();
            echo "\$customer_group = new \App\Models\CustomerGroup();\r\n";
            foreach($datas as $column => $value)
            {
                if(!is_null($value))
                    echo "\$customer_group->{$column} = '$value';\r\n";
            }
            echo "\$customer_group->save();\r\n\r\n";
        }
        echo '</pre>';
    }

    protected function paymentsSeed()
    {
        $payments = Payment::all();
        echo '<pre>';
        foreach($payments as $payment)
        {
            $datas = $payment->toArray();
            echo "\$payment = new \App\Models\Payment();\r\n";
            foreach($datas as $column => $value)
            {
                if(!is_null($value))
                    echo "\$payment->{$column} = '$value';\r\n";
            }
            echo "\$payment->save();\r\n\r\n";
        }
        echo '</pre>';
    }

    /******************** Insertion methods ********************/
    protected function InsertUsers()
    {
        $user = new \App\User();
        $user->id = '2';
        $user->first_name = 'Ricardo';
        $user->last_name = 'Magnusson';
        $user->phone = '19997535875';
        $user->age = '31';
        $user->city = 'Piracicaba';
        $user->state = 'São Paulo';
        $user->active = '1';
        $user->email = 'ricardo.magnusson@gmail.com';
        $user->password = '$2y$10$8n2RDQO6nD2qLI5BhMC/kOZYR6G5xzsKrNWgflg/Yl1zFbNRXEPSa';
        $user->remember_token = 'O6UGzID64Pg4N6HWEJdZPqR4pHwMBqUTlXrICD57iFvfssuPFOuzaJ1WdiFS';
        $user->created_by = '0';
        $user->created_at = '2018-11-05 21:28:29';
        $user->updated_at = '2018-11-05 21:28:29';
        $user->save();

        $user = new \App\User();
        $user->id = '3';
        $user->first_name = 'Paulo Cesar';
        $user->last_name = 'Magnusson';
        $user->phone = '19997744062';
        $user->age = '57';
        $user->city = 'Indaiatuba';
        $user->state = 'São Paulo';
        $user->active = '1';
        $user->email = 'drpaulo@ctoindaia.com.br';
        $user->password = '$2y$10$Z.b7tCdomNePYHwSg33LSeCyXaAy76zHA08ZLCTiO0NR6Luarpbn.';
        $user->created_by = '2';
        $user->created_at = '2018-11-12 14:41:48';
        $user->updated_at = '2018-11-12 14:41:48';
        $user->save();

        $user = new \App\User();
        $user->id = '4';
        $user->first_name = 'Giovanni';
        $user->last_name = 'Ferrari';
        $user->phone = '19974087464';
        $user->age = '31';
        $user->city = 'Piracicaba';
        $user->state = 'São Paulo';
        $user->active = '1';
        $user->email = 'giovanni@piracapas.com.br';
        $user->password = '$2y$10$mt/Hqpf2IauCoAfS24eQfe17VcjH4EvHDvEcMBgMXjDfsI9Z9ZYk6';
        $user->created_by = '2';
        $user->created_at = '2018-11-12 14:42:39';
        $user->updated_at = '2018-11-12 14:42:39';
        $user->save();

        $user = new \App\User();
        $user->id = '5';
        $user->first_name = 'Ivânio';
        $user->last_name = 'Formighieri';
        $user->phone = '+595984777321';
        $user->age = '22';
        $user->city = 'Ciudad del Este';
        $user->state = 'Paraguai';
        $user->active = '1';
        $user->email = 'ivanio@formighieri.com';
        $user->password = '$2y$10$u1GxulnRqz.VD0MF82WVSuBw8JraFvV/qW/6uBLqdU3Orjd/cWTk2';
        $user->created_by = '2';
        $user->created_at = '2018-11-12 14:44:11';
        $user->updated_at = '2018-11-12 14:44:11';
        $user->save();

        $user = new \App\User();
        $user->id = '6';
        $user->first_name = 'Vilmar';
        $user->last_name = 'formighieri';
        $user->phone = '+595983693400';
        $user->age = '48';
        $user->city = 'Ciudad del Este';
        $user->state = 'Paraguai';
        $user->active = '1';
        $user->email = 'formighieri@formighieri.com';
        $user->password = '$2y$10$kepEn7x.2Jeeugaej2JS1ODQaXqn5M.pAk7uiquVjXq9GzfK1pOTC';
        $user->created_by = '2';
        $user->created_at = '2018-11-12 14:45:10';
        $user->updated_at = '2018-11-12 14:45:10';
        $user->save();

        $user = new \App\User();
        $user->id = '7';
        $user->first_name = 'Ricardo';
        $user->last_name = 'Magnusson';
        $user->phone = '19998875875';
        $user->age = '31';
        $user->city = 'Piracicaba';
        $user->state = 'São Paulo';
        $user->active = '1';
        $user->email = 'ricardo@grupoperfuratrizdth.com.br';
        $user->password = '$2y$10$Ge/Qw5BH51DL9M4fQZEXp.5RsLEd51luPc3Xu4C1VoQVjyMVS3z0q';
        $user->created_by = '2';
        $user->created_at = '2018-11-19 11:31:51';
        $user->updated_at = '2018-11-19 11:31:51';
        $user->save();

        $user = new \App\User();
        $user->id = '8';
        $user->first_name = 'Rafael';
        $user->last_name = 'Oliveira da Silva';
        $user->phone = '19983999450';
        $user->age = '34';
        $user->city = 'Piracicaba';
        $user->state = 'São Paulo';
        $user->active = '1';
        $user->email = 'rafa.oliveirasilva@yahoo.com.br';
        $user->password = '$2y$10$fwullKpSISJTH4hV8vTN7.sO4aSyudglaI1d38vKDySsqj8dLxFGm';
        $user->created_by = '2';
        $user->created_at = '2018-11-19 11:33:51';
        $user->updated_at = '2018-11-19 11:33:51';
        $user->save();
    }

    protected function InsertDestinations()
    {
        $destination = new \App\Models\Destination();
        $destination->id = '1';
        $destination->name = 'Dodarya';
        $destination->city = 'Karachi';
        $destination->max_capacity = '10000';
        $destination->price = '50';
        $destination->active = '1';
        $destination->created_by = '1';
        $destination->created_at = '2018-11-05 20:36:51';
        $destination->updated_at = '2018-11-05 20:36:51';
        $destination->save();

        $destination = new \App\Models\Destination();
        $destination->id = '3';
        $destination->name = 'Pousada Juruena';
        $destination->city = 'Mato Grosso';
        $destination->max_capacity = '34';
        $destination->price = '6030';
        $destination->active = '1';
        $destination->created_by = '2';
        $destination->created_at = '2018-11-05 23:07:16';
        $destination->updated_at = '2018-11-19 11:35:00';
        $destination->save();

        $destination = new \App\Models\Destination();
        $destination->id = '4';
        $destination->name = 'Barco Tayaçu II';
        $destination->city = 'Barcelos';
        $destination->max_capacity = '10';
        $destination->price = '6970';
        $destination->active = '1';
        $destination->created_by = '2';
        $destination->created_at = '2018-12-10 23:47:05';
        $destination->updated_at = '2018-12-10 23:47:58';
        $destination->save();

        $destination = new \App\Models\Destination();
        $destination->id = '5';
        $destination->name = 'Barco Açu 3';
        $destination->city = 'Barcelos';
        $destination->max_capacity = '12';
        $destination->price = '6970';
        $destination->active = '1';
        $destination->created_by = '2';
        $destination->created_at = '2018-12-10 23:47:49';
        $destination->updated_at = '2018-12-10 23:47:49';
        $destination->save();

    }

    protected function InsertGroups()
    {
        $group = new \App\Models\Group();
        $group->id = '1';
        $group->destination_id = '1';
        $group->arrival_date = '2018-11-01 00:00:00';
        $group->out_date = '2018-11-06 00:00:00';
        $group->active = '1';
        $group->created_by = '1';
        $group->created_at = '2018-11-05 20:37:40';
        $group->updated_at = '2018-11-24 17:37:58';
        $group->special_requirements = 'This is testing...';
        $group->arrival_hour = '15:06';
        $group->save();

        $group = new \App\Models\Group();
        $group->id = '3';
        $group->destination_id = '3';
        $group->arrival_date = '2019-07-20 00:00:00';
        $group->out_date = '2019-07-26 00:00:00';
        $group->active = '1';
        $group->created_by = '2';
        $group->created_at = '2018-11-05 23:08:03';
        $group->updated_at = '2018-11-05 23:08:03';
        $group->save();
    }

    protected function InsertCustomersGroup()
    {
        $customer_group = new \App\Models\CustomerGroup();
        $customer_group->group_id = '1';
        $customer_group->customer_id = '1';
        $customer_group->price = '50.00';
        $customer_group->flight_no = 'TEST-12345';
        $customer_group->save();

        $customer_group = new \App\Models\CustomerGroup();
        $customer_group->group_id = '2';
        $customer_group->customer_id = '2';
        $customer_group->price = '6950.00';
        $customer_group->save();

        $customer_group = new \App\Models\CustomerGroup();
        $customer_group->group_id = '2';
        $customer_group->customer_id = '1';
        $customer_group->price = '6500.00';
        $customer_group->save();

        $customer_group = new \App\Models\CustomerGroup();
        $customer_group->group_id = '3';
        $customer_group->customer_id = '3';
        $customer_group->price = '6030.00';
        $customer_group->flight_no = '4166';
        $customer_group->arrival_hour = '13:55';
        $customer_group->save();

        $customer_group = new \App\Models\CustomerGroup();
        $customer_group->group_id = '3';
        $customer_group->customer_id = '4';
        $customer_group->price = '6030.00';
        $customer_group->flight_no = '4166';
        $customer_group->arrival_hour = '13:55';
        $customer_group->save();

        $customer_group = new \App\Models\CustomerGroup();
        $customer_group->group_id = '3';
        $customer_group->customer_id = '5';
        $customer_group->price = '6030.00';
        $customer_group->save();

        $customer_group = new \App\Models\CustomerGroup();
        $customer_group->group_id = '3';
        $customer_group->customer_id = '6';
        $customer_group->price = '6030.00';
        $customer_group->save();

        $customer_group = new \App\Models\CustomerGroup();
        $customer_group->group_id = '3';
        $customer_group->customer_id = '7';
        $customer_group->price = '6030.00';
        $customer_group->flight_no = '4166';
        $customer_group->arrival_hour = '13:55';
        $customer_group->save();

        $customer_group = new \App\Models\CustomerGroup();
        $customer_group->group_id = '3';
        $customer_group->customer_id = '8';
        $customer_group->price = '6030.00';
        $customer_group->flight_no = '4166';
        $customer_group->arrival_hour = '13:55';
        $customer_group->save();
    }

    protected function InsertPayments()
    {
        $payment = new \App\Models\Payment();
        $payment->id = '1';
        $payment->group_id = '1';
        $payment->customer_id = '1';
        $payment->amount = '40.00';
        $payment->proof = 'proofs/cvnHdHNlMZYepwoHHBqfhCl5N83F02GBDOTCLXva.jpeg';
        $payment->created_by = '1';
        $payment->created_at = '2018-11-05 20:38:13';
        $payment->updated_at = '2018-11-05 20:38:13';
        $payment->save();

        $payment = new \App\Models\Payment();
        $payment->id = '2';
        $payment->group_id = '1';
        $payment->customer_id = '1';
        $payment->amount = '5.00';
        $payment->proof = 'proofs/EUqm9q4ukPQ11hVa4wlAaWKg68NAKHA33c4aZHqp.jpeg';
        $payment->created_by = '1';
        $payment->created_at = '2018-11-05 20:38:42';
        $payment->updated_at = '2018-11-25 13:49:15';
        $payment->payment_method = 'null';
        $payment->remarks = 'null';
        $payment->save();

        $payment = new \App\Models\Payment();
        $payment->id = '3';
        $payment->group_id = '1';
        $payment->customer_id = '1';
        $payment->amount = '2.00';
        $payment->proof = 'proofs/Ctxv6ZAA9G23P1n59kl2nZ85e9FVXpe0Lxa6jViD.jpeg';
        $payment->created_by = '1';
        $payment->created_at = '2018-11-05 20:39:12';
        $payment->updated_at = '2018-11-05 21:26:32';
        $payment->save();

        $payment = new \App\Models\Payment();
        $payment->id = '4';
        $payment->group_id = '1';
        $payment->customer_id = '1';
        $payment->amount = '3.00';
        $payment->proof = 'proofs/Oatd7EcV1nS5DENgozB666jjjntFuEC034S7fCIj.jpeg';
        $payment->created_by = '1';
        $payment->created_at = '2018-11-05 21:26:59';
        $payment->updated_at = '2018-11-05 21:26:59';
        $payment->save();

        $payment = new \App\Models\Payment();
        $payment->id = '5';
        $payment->group_id = '2';
        $payment->customer_id = '2';
        $payment->amount = '3200.00';
        $payment->proof = 'proofs/y6RMGTzIAZOdefSZb6GbV1BZVLOUu44tsnXvADxB.png';
        $payment->created_by = '2';
        $payment->created_at = '2018-11-05 21:34:14';
        $payment->updated_at = '2018-11-05 21:34:14';
        $payment->save();

        $payment = new \App\Models\Payment();
        $payment->id = '6';
        $payment->group_id = '3';
        $payment->customer_id = '3';
        $payment->amount = '603.00';
        $payment->proof = 'proofs/2IfZkvLuctQuE5TFEgexmywg5VLzXZEEfHlYnvmR.png';
        $payment->created_by = '2';
        $payment->created_at = '2018-11-20 10:43:42';
        $payment->updated_at = '2018-11-20 10:43:42';
        $payment->save();

        $payment = new \App\Models\Payment();
        $payment->id = '7';
        $payment->group_id = '3';
        $payment->customer_id = '8';
        $payment->amount = '603.00';
        $payment->proof = 'proofs/AviT8QdbVUHBuHH5MRkg6JHcAVXpNK2SBBHZwptC.jpeg';
        $payment->created_by = '2';
        $payment->created_at = '2018-11-21 21:56:59';
        $payment->updated_at = '2018-11-21 21:56:59';
        $payment->save();

        $payment = new \App\Models\Payment();
        $payment->id = '8';
        $payment->group_id = '3';
        $payment->customer_id = '4';
        $payment->amount = '1575.00';
        $payment->proof = 'proofs/m7MYEDfLGZzSuxp27dKXHAM8TQY0lYSO12Dg1HKU.jpeg';
        $payment->created_by = '2';
        $payment->created_at = '2018-11-23 10:07:00';
        $payment->updated_at = '2018-11-23 10:07:00';
        $payment->save();

        $payment = new \App\Models\Payment();
        $payment->id = '9';
        $payment->group_id = '3';
        $payment->customer_id = '7';
        $payment->amount = '1575.00';
        $payment->proof = 'proofs/IQJz47XdGIoackyZr3dkIJSpGxO2wY2Go0NrgF7m.jpeg';
        $payment->created_by = '2';
        $payment->created_at = '2018-11-23 10:07:31';
        $payment->updated_at = '2018-11-23 10:07:31';
        $payment->save();

        $payment = new \App\Models\Payment();
        $payment->id = '10';
        $payment->group_id = '3';
        $payment->customer_id = '5';
        $payment->amount = '603.00';
        $payment->proof = 'proofs/jJMCdeuCwoLaSbAHk9JLs3JV8Tsto7dwYv49defB.jpeg';
        $payment->created_by = '2';
        $payment->created_at = '2018-11-27 17:05:01';
        $payment->updated_at = '2018-11-27 17:05:20';
        $payment->payment_method = 'Bank Transfer';
        $payment->remarks = '.';
        $payment->save();
    }
}
