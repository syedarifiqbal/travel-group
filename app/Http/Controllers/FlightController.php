<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;

class FlightController extends Controller
{
    public function getCustomerFlight(Request $request)
    {
        return Group::findOrFail($request->group_id)->customers()->where('id', $request->customer_id)->first()->pivot;
    }
}
