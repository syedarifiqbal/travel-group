<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        \DB::listen(function($q){ dump($q); });

        $data  = [
            'title' => 'Dashboard',
            'breadcrumb' => 'home',
            'paid' => auth()->user()->customer_payments()->selectRaw('sum(amount) as amount')->first(),
            'total' => collect(\DB::select("SELECT SUM(price) AS amount FROM `customer_group` AS cg JOIN groups as g ON g.id = cg.group_id WHERE g.`created_by` = " . auth()->id() . " AND g.`created_by` IS NOT NULL"))->first(),
            'my_customers_count' => auth()->user()->customers()->count(),
            'my_group_customers_count' => collect(\DB::select("SELECT COUNT(DISTINCT cg.customer_id) AS customer_count FROM customer_group as cg JOIN groups AS g ON g.id = cg.group_id WHERE g.`created_by` = " . auth()->id() . " AND g.`created_by` IS NOT NULL"))->first()->customer_count
        ];

        return view('home', $data);
    }
}
