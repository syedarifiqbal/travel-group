<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;

class CustomerGroupController extends Controller
{

    public function getCustomers(Request $request, Group $group)
    {
        $group->load(['customers' => function($q) use($group){
            $q->with(['payments' => function($q) use($group){
                $q->where('group_id', $group->id);
            }]);
        }]);
        return $group;
    }

    public function storeCustomerFlight(Request $request, Group $group)
    {
        $request->validate(['flight_no' => 'required']);
        $pivotData = [];
        foreach ($group->customers as $customer)
        {
            $pivotData[$customer->id] = [
                'price' => $customer->pivot->price,
                'flight_no' => $customer->id == $request->customer_id? $request->flight_no: $customer->pivot->flight_no,
                'arrival_hour' => $customer->id == $request->customer_id? $request->arrival_hour: $customer->pivot->arrival_hour,
            ];
        }
        $group->customers()->sync($pivotData);
        return response()->make(['status' => true, 'message' => 'flight number is saved']);
    }

}
