<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function groupPaymentFrom(Group $group)
    {
        $data = [
            'group' => $group,
            'payment' => new Payment()
        ];
        return view('groups.paymentForm', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Models\Group  $group
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Group $group)
    {
        $validatedData = $request->validate([
            'customer_id' => 'required',
            'amount' => 'required',
            'proof' => 'required|file',
            'payment_method' => 'required',
            'remarks' => 'required',
        ]);
        $validatedData['proof'] = $request->proof->store('proofs');
        $payment = new Payment($validatedData);
        $payment->creator()->associate(auth()->user());
        $group->payments()->save($payment);
        return response()->make([
            'status' => 'success',
            'message' => 'Payment is received successfully!',
            'returnUrl' => route('groups.show', ['group' => $group->id])
        ], 201);

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Request $request
     * @param  \App\Models\Payment  $payment
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Group $group, Payment $payment)
    {
        if ($request->isXmlHttpRequest())
        {
            return $payment;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group, Payment $payment)
    {
        return view('groups.paymentForm', ['group' => $group, 'payment' => $payment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Request $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group, Payment $payment)
    {
        $validatedData = $request->validate([
            'customer_id' => 'required',
            'amount' => 'required',
            'payment_method' => 'required',
            'remarks' => 'required',
        ]);
        if ($request->hasFile('proof'))
        {
            $validatedData['proof'] = $request->proof->store('proofs');
        }
        $payment->fill($validatedData);
        $group->payments()->save($payment);
        return response()->make([
            'status' => 'success',
            'message' => 'Payment is received successfully!',
            'returnUrl' => route('groups.show', ['group' => $group->id])
        ], 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }

    /**
     * Download Proof file from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function downloadProof(Payment $payment)
    {
        return response()->download(storage_path("app/{$payment->proof}"));
    }

    /**
     * Download Proof file from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function customerBalance(Request $request)
    {
        $group = Group::findOrFail($request->group_id);
        $amount = $group->customers()->where('id', $request->customer_id)->first()->pivot->price;
        $received = $group->payments()
                        ->where('customer_id', $request->customer_id)
                        ->where('id', '!=', $request->payment_id)
                        ->get()->sum('amount');
        return $amount - $received;
    }
}
