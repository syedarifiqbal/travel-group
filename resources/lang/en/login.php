<?php

return [
    'title' => 'Sign in To Account',
    'forgot_password' => 'Forgot Password ?',
    'sign_in' => 'Sign In',
    'privacy' => 'Privacy',
    'legal' => 'Legal',
    'contact' => 'Contact',
    'sign_up' => 'Sign Up',
    'admin' => [
        'title' => 'Admin Sign in'
    ],

];