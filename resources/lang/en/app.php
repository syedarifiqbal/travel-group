<?php

return [
    'name' => 'Travel Groups',
    'admin' => 'Admin',
    'register' => 'Register Manager',
    'total_customers' => 'Total Customers',
    'dashboard' => 'Dashboard',
    'admin_dashboard' => 'Admin Dashboard',
    'total_in_groups' => 'Total in Groups',
    'destinations' => 'Destination',
    'destinations_list' => 'Destination List',
    'new_destinations' => 'New Destination',
    'groups' => 'Groups',
    'groups_list' => 'Groups List',
    'new_group' => 'New Group',
    'edit' => 'Edit',
    'new' => 'New',
    'view_list' => 'View List',
    'arrival_date' => 'Arrival Date',
    'arrival_time' => 'Arrival Time',
    'special_requirements' => 'Special Requirements',
    'group_owner' => 'Group Owner',
    'out_date' => 'Out Date',
    'customer' => 'Customer',
    'total_price' => 'Total Price',
    'price' => 'Price',
    'save' => 'Save',
    'saving' => 'Saving',
    'customers' => 'Customers',
    'customers_list' => 'Customers List',
    'new_customer' => 'New Customer',
    'reference' => 'Reference',
    'action' => 'Action',
    'no_customers' => 'No. Customers',
    'total_clients' => 'Total Clients',
    'group_detail' => 'Group Detail',
    'payment_received' => 'Payment has been received',
    'payment' => 'Payment',
    'payment_for' => 'Payment',
    'amount' => 'Amount',
    'attach_payment_proof' => 'Attach Payment Proof',
    'comments' => 'Comments',
    'bank_transfer' => 'Bank Transfer',
    'money' => 'Money',
    'bank_slip' => 'Bank slip',
    'payment_method' => 'Payment Method',
    'loading' => 'Loading',
    'balance' => 'Balance',
    'register_new_payment' => 'Register new Payment',
    'phone' => 'Phone',
    'email' => 'Email',
    'managers' => 'Managers',
    'create_manager' => 'Create Managers',
    'manager_list' => 'Managers List',
    'new_manager' => 'New Manager',
    'details' => 'Details',
    'clients' => 'Client',
    'payment_list' => 'Payments',
    'flight' => 'Flight',
    'flight_no' => 'Flight no',
    'flight_number' => 'Flight number',
    'total_paid' => 'Total Paid',
    'total' => 'Total',
    'paid' => 'Paid',
    'total_received' => 'Total Received',
    'arrival_hours' => 'Arrival Hours',
    'download_payment_proof' => 'Download Payment Proof',
    'payment_id' => 'Payment ID',
    'date' => 'Date',
    'receipt_date' => 'Receipt Date',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'city' => 'City',
    'date_of_birth' => 'Date or Birth',
    'state' => 'State',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'delete' => 'Delete',
    'max_capacity' => 'Max Capacity',
    'status' => 'Status',
    'active' => 'Active',
    'disable' => 'Disable',

];
