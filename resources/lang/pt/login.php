<?php

return [
    'title' => 'Assine para a conta',
    'forgot_password' => 'Esqueceu a senha ?',
    'sign_in' => 'Assinar em',
    'privacy' => 'Privacidade',
    'legal' => 'Legal',
    'contact' => 'Contato',
    'sign_up' => 'Inscrever-se',
    'admin' => [
        'title' => 'Admin Sign in'
    ],

];