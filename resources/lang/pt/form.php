<?php

return [
    'name' => 'Nome',
    'city' => 'Cidade',
    'max_capacity' => 'Capacidade máxima',
    'price' => 'Preço',
    'submit' => 'Enviar',
    'cancel' => 'Cancelar',

];
