@extends('layouts.app')

@section('footer_scripts')
    <!--begin::Page Vendors -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/datatables/basic/basic.js') }}" type="text/javascript"></script>
@endsection

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.customers_list') }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('customers') !!}
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('app.customers_list') }}
                    <a href="{{ route('customers.create') }}" class="btn btn-success float-right">{{ __('app.new_customer') }}</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped- table-bordered table-hover table-checkable" data-sort-column="2" data-sort-order="asc" id="k_table_1">
                        <thead>
                        <tr>
                            <th>{{ __('app.reference') }}</th>
                            <th>{{ __('app.name') }}</th>
                            <th>{{ __('app.email') }}</th>
                            <th>{{ __('app.phone') }}</th>
                            <th>{{ __('app.city') }}</th>
                            {{--<th>{{ __('app.action') }}</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $row)
                            <tr>
                                <td>{{ $row->id }}</td>
                                <td>{{ $row->first_name . ' ' . $row->first_name }}</td>
                                <td>{{ $row->email }}</td>
                                <td>{{ $row->phone }}</td>
                                <td>{{ $row->city }}</td>
                                {{--<td>
                                    <span class="dropdown show">
                                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                                        <i class="la la-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-218px, 25px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <a class="dropdown-item" href="{{ route('groups.edit', ['group' => $row->id ]) }}"><i class="la la-edit"></i> Edit</a>
                                            <a class="dropdown-item" href="{{ route('groups.payments.create', ['group' => $row->id ]) }}"><i class="la la-money"></i> Payment</a>
                                            <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('row{{ $row->id }}').submit();"><i class="la la-trash"></i>
                                                <form action="{{ route('groups.destroy', $row->id) }}" id="row{{ $row->id }}" class="d-inline" method="post">
                                                    @csrf
                                                    @method('delete')
                                                </form>
                                                Delete
                                            </a>
                                        </div>
                                    </span>

                                </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
