@extends('layouts.app')

@section('footer_scripts')
    <script src="{{ asset('assets/app\scripts\custom\dashboard.js') }}" type="text/javascript"></script>
@endsection

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.dashboard') }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('home') !!}
@endsection

@section('content')

<div class="container">

    <div class="row">

        <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
            <div class="k-portlet k-portlet--height-fluid k-widget-10">
                <div class="k-portlet__body">
                    <div class="k-widget-10__wrapper">
                        <div class="k-widget-10__details">
                            <div class="k-widget-10__title">{{ __('app.total_customers') }}</div>
                            {{--<div class="k-widget-10__desc">28 Daily Avr</div>--}}
                        </div>
                        <div class="k-widget-10__num">
                            {{ $my_customers_count }}
                        </div>
                    </div>
                </div>
                <div class="k-portlet__body k-portlet__body--fit">
                    <div class="k-widget-10__chart">
                        <!--Doc: For the chart initialization refer to "mediumCharts" function in "src\theme\app\scripts\custom\dashboard.js" -->
                        <canvas id="k_widget_mini_chart_1" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
            <div class="k-portlet k-portlet--height-fluid k-widget-10">
                <div class="k-portlet__body">
                    <div class="k-widget-10__wrapper">
                        <div class="k-widget-10__details">
                            <div class="k-widget-10__title">{{ __('app.total_in_groups') }}</div>
                            {{--<div class="k-widget-10__desc">28 Daily Avr</div>--}}
                        </div>
                        <div class="k-widget-10__num">
                            {{ $my_customers_count }}
                        </div>
                    </div>
                </div>
                <div class="k-portlet__body k-portlet__body--fit">
                    <div class="k-widget-10__chart">
                        <!--Doc: For the chart initialization refer to "mediumCharts" function in "src\theme\app\scripts\custom\dashboard.js" -->
                        <canvas id="k_widget_mini_chart_2" height="100"></canvas>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<div class="container">
    <div class="row">

        {{--<div class="col-md-4">
            <div class="widget widget-stats bg-green">
                <div class="stats-icon"><i class="fa fa-users"></i></div>
                <div class="stats-info">
                    <h4>TOTAL USERS</h4>
                    <p>{{ $my_customers_count }}</p>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-user-circle-o"></i></div>
                <div class="stats-info">
                    <h4>TOTAL IN {{ strtoupper(__('app.groups')) }}</h4>
                    <p>{{ $my_group_customers_count }}</p>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="widget widget-stats bg-red">
                <div class="stats-icon"><i class="fa fa-plus-circle"></i></div>
                <div class="stats-info">
                    <h4>&nbsp;</h4>
                    <a href="{{ route('customers.create') }}" class="text-white">
                        <p>REGISTER NEW CUSTOMER</p>
                    </a>
                </div>
                <div class="stats-link">
                    <a href="javascript:;">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
            </div>
        </div>--}}
    </div>
    <div class="row mt-5 justify-content-center">

        <div class="col-md-3">
            <div id="paid" data-amount="{{ $total->amount>0? $paid->amount/$total->amount*100:0 }}"></div>
            <h3 class="text-primary text-muted text-center mt-4">{{ __('app.total_paid') }}</h3>
        </div>

        <div class="col-md-3 offset-md-1">
            <div id="remain" data-amount="{{ $total->amount>0? ($total->amount - $paid->amount)/$paid->amount*100:0 }}"></div>
            <h3 class="text-primary text-muted text-center mt-4">{{ __('app.total_received') }}</h3>
        </div>

        <div class="col-md-3 offset-md-1">
            <h3>{{ __('app.total') }}</h3>
            <h2 class="text-secondary font-weight-bold">R$ {{ $total->amount }}</h2>
            <h3>{{ __('app.paid') }}</h3>
            <h2 class="text-primary font-weight-bold">R$ {{ $paid->amount }}</h2>
            <h3>{{ __('app.balance') }}</h3>
            <h2 class="text-danger font-weight-bold">R$ {{ $total->amount - $paid->amount }}</h2>
            <a class="btn btn-success btn-lg" href="{{ route('groups.index') }}">
                <i class="fa fa-plus-circle" aria-hidden="true"></i> {{ __('app.register_new_payment') }}
            </a>
        </div>
    </div>

</div>
@endsection

@section('footer-script')

    <script>

    </script>

@endsection