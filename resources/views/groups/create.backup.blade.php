@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $group->id? 'Edit': 'New' }} Group
                        <a class="btn btn-success float-right" href="{{ route('groups.index') }}">View List</a>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ $group->id? route('groups.update', ['destination' => $group->id]): route('groups.store') }}">
                            @csrf
                            @if($group->id) @method('put') @endif

                            <div class="form-group row">
                                <label for="arrival_date" class="col-md-4 col-form-label text-md-right">Arrival Date</label>

                                <div class="col-md-6">
                                    <input id="arrival_date" type="date" class="form-control{{ $errors->has('arrival_date') ? ' is-invalid' : '' }}" name="arrival_date" value="{{ old('arrival_date', $group->arrival_date? $group->arrival_date->format('Y-m-d'): '') }}" required autofocus>

                                    @if ($errors->has('arrival_date'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('arrival_date') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="out_date" class="col-md-4 col-form-label text-md-right">Out Date</label>

                                <div class="col-md-6">
                                    <input id="out_date" type="date" class="form-control{{ $errors->has('out_date') ? ' is-invalid' : '' }}" name="out_date" value="{{ old('out_date', $group->out_date? $group->out_date->format('Y-m-d'): '') }}" required autofocus>

                                    @if ($errors->has('out_date'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('out_date') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="go" class="col-md-4 col-form-label text-md-right">Group Owner</label>

                                <div class="col-md-6">
                                    <input id="go" type="text" readonly class="form-control" value="{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}" required>
                                </div>
                            </div>

                            <div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Customer</th>
                                        <th>Total Price</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <input type="hidden" v-model="customerValues" name="customers">
                                    <tr v-for="(customer, index) in customers">
                                        <td>
                                            <v-select label="first_name" name="customers[]" :options="options" v-model="customer.name"></v-select>
                                        </td>
                                        <td>
                                            <input type="number" v-model="customer.price" class="form-control">
                                        </td>
                                        <td>
                                            <button class="btn btn-danger" type="button" @click="deleteCustomer(index)" v-if="customers.length>1">-</button>
                                            <button class="btn btn-primary" type="button" @click="addCustomer()">+</button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
