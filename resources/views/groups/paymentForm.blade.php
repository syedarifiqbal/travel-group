@extends('layouts.app')
@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.register_new_payment') }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('groups.payment', $group) !!}
@endsection
@section('content')
    <group-payment-form group-id="{{ $group->id }}"  @if($payment->id) payment-id="{{ $payment->id }}" @endif></group-payment-form>
@endsection
