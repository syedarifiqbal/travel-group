@extends('layouts.app')

@section('footer_scripts')
    <!--begin::Page Vendors -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/datatables/basic/basic.js') }}" type="text/javascript"></script>
@endsection

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.groups_list') }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('groups') !!}
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('app.groups_list') }}
                    <a href="{{ route('groups.create') }}" class="btn btn-success float-right">{{ __('app.new_group') }}</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped- table-bordered table-hover table-checkable" id="k_table_1">
                        <thead>
                        <tr>
                            <th>{{ __('app.reference') }}</th>
                            <th>{{ __('app.destinations') }}</th>
                            <th>{{ __('app.arrival_date') }}</th>
                            <th>{{ __('app.out_date') }}</th>
                            <th>{{ __('app.total_clients') }}</th>
                            <th>{{ __('app.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($groups as $row)
                            <tr>
                                <td><a href="{{ route('groups.show', ['group'=>$row->id]) }}">{{ $row->id }}</a></td>
                                <td><a href="{{ route('groups.show', ['group'=>$row->id]) }}">{{ $row->destination->name }}</a></td>
                                <td><a href="{{ route('groups.show', ['group'=>$row->id]) }}">{{ $row->arrival_date->format('d/m/Y') }}</a></td>
                                <td><a href="{{ route('groups.show', ['group'=>$row->id]) }}">{{ $row->out_date->format('d/m/Y') }}</a></td>
                                <td><a href="{{ route('groups.show', ['group'=>$row->id]) }}">{{ $row->customers_count }}</a></td>
                                <td>
                                    <a href="{{ route('groups.edit', ['group' => $row->id ]) }}"><i class="fa fa-edit btn btn-outline-info"></i> </a>
                                    <a href="{{ route('groups.payments.create', ['group' => $row->id ]) }}"><i class="fa fa-money btn btn-outline-info"></i> {{-- __('app.payment') --}}</a>
                                    <a href="#" onclick="event.preventDefault(); document.getElementById('row{{ $row->id }}').submit();"><i class="fa fa-trash btn btn-outline-danger"></i>
                                        <form action="{{ route('groups.destroy', $row->id) }}" id="row{{ $row->id }}" class="d-inline" method="post">
                                            @csrf
                                            @method('delete')
                                        </form>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
