@extends('layouts.app')

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.groups') }} {{ $group->destination->name }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('groups.show', $group) !!}
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $group->destination->name }} [{{ $group->out_date->format('m/d/Y') }} - {{ $group->arrival_date->format('m/d/Y') }}]</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul class="nav nav-pills nav-tabs-btn" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#k_tabs_8_1" role="tab">
                                <span class="nav-link-icon"><i class="flaticon2-graph-1"></i></span>
                                <span class="nav-link-title">{{ __('app.details') }}</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#k_tabs_8_2" role="tab">
                                <span class="nav-link-icon"><i class="flaticon2-position"></i></span>
                                <span class="nav-link-title">{{ __('app.clients') }}</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#k_tabs_8_3" role="tab">
                                <span class="nav-link-icon"><i class="flaticon2-placeholder"></i></span>
                                <span class="nav-link-title">{{ __('app.payment_list') }}</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="k_tabs_8_1" role="tabpanel">
                            <ul class="list-group">
                                <li class="list-group-item">Destination: {{ $group->destination->name }}</li>
                                <li class="list-group-item">Arrival Date: {{ $group->arrival_date->format('d/m/Y') }}</li>
                                <li class="list-group-item">Out Date: {{ $group->out_date->format('d/m/Y') }}</li>
                                <li class="list-group-item">No. Members: {{ count($group->customers) }}</li>
                                <li class="list-group-item">Owner: {{ $group->creator->first_name }} {{ $group->creator->last_name }}</li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="k_tabs_8_2" role="tabpanel">

                            <group-customer-list :group-id="{{ $group->id }}"></group-customer-list>

                        </div>
                        <div class="tab-pane fade" id="k_tabs_8_3" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>{{ __('app.payment_id') }}</th>
                                        <th>{{ __('app.receipt_date') }}</th>
                                        <th>{{ __('app.customer') }}</th>
                                        <th>{{ __('app.amount') }}</th>
                                        <th>{{ __('app.action') }}</th>
                                    </tr>
                                    @foreach($group->payments()->with('customer')->get() as $payment)
                                        <tr>
                                            <td>{{ $payment->id }}</td>
                                            <td>{{ $payment->created_at->format('d/m/Y') }}</td>
                                            <td>{{ $payment->customer->first_name }} {{ $payment->customer->last_name }}</td>
                                            <td>{{ $payment->amount }}</td>
                                            <td>
                                                <a class="fa fa-edit btn btn-outline-info" href="{{ route('groups.payment.edit', ['group' => $payment->group_id, 'payment' => $payment->id]) }}"></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="k-portlet">
                <div class="k-portlet__head">
                    <div class="k-portlet__head-label">
                        <h3 class="k-portlet__head-title"></h3>
                    </div>
                </div>
                <div class="k-portlet__body">
                    
                </div>
            </div>

        </div>

    </div>

    <div class="modal" id="flightModal" tabindex="-1" role="dialog">
        <form class="modal-dialog" role="document" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Flight Number</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="flight_no">Flight no</label>
                        <input type="text" class="form-control" id="flight_no" name="flight_no" placeholder="Enter Flight Number">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
@endsection
