@extends('layouts.app')

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.new_group') }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('groups.create', $group->id) !!}
@endsection

@section('content')
    <create-group @if($group->id) group-id="{{ $group->id }}" @endif></create-group>
@endsection
