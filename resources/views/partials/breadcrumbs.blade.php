@if (count($breadcrumbs))

    <div class="k-content__head-breadcrumbs">
        @foreach ($breadcrumbs as $breadcrumb)

            @if ($breadcrumb->url && !$loop->last)
                <a href="{{ $breadcrumb->url }}" class="k-content__head-breadcrumb-link">{!! $breadcrumb->title !!}</a>
                <span class="k-content__head-breadcrumb-separator"></span>
            @else
                <span class="k-content__head-breadcrumb-link k-content__head-breadcrumb-link--active">{!! $breadcrumb->title !!}</span>
            @endif

        @endforeach
    </div>

@endif

