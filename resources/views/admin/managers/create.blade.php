@extends('admin.layouts.app')

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.new_group') }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('managers.create') !!}
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="k-portlet">
                <div class="k-portlet__head">
                    <div class="k-portlet__head-label">
                        <h3 class="k-portlet__head-title">{{ __('app.new_manager') }}</h3>
                    </div>
                </div>
                <!--begin::Form-->
                <form method="POST" class="k-form" action="{{ $manager->id? route('managers.update', ['manager' => $manager->id]): route('managers.store') }}">
                    @csrf
                    @if($manager->id) @method('put') @endif

                    <div class="k-portlet__body">

                        <div class="form-group validated">
                            <label for="name">{{ __('form.name') }}</label>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $manager->first_name) }}" required autofocus>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $manager->email) }}" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        @if(!$manager->id)
                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                        @endif

                    </div>
                    <div class="k-portlet__foot">
                        <div class="k-form__actions k-form__actions">
                            <button type="submit" class="btn btn-brand">{{ __('form.submit') }}</button>
                            <a href="{{ route('destinations.index') }}" class="btn btn-secondary">{{ __('form.cancel') }}</a>
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

@endsection
