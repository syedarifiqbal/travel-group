@extends('admin.layouts.app')

@section('footer_scripts')
    <!--begin::Page Vendors -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/datatables/basic/basic.js') }}" type="text/javascript"></script>
@endsection

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.manager_list') }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('managers') !!}
@endsection

@section('content')
    <div class="k-content__body	k-grid__item k-grid__item--fluid" id="k_content_body">

        <div class="k-portlet k-portlet--mobile">
            <div class="k-portlet__head">
                <div class="k-portlet__head-label">
                    <h3 class="k-portlet__head-title">{{ __('app.manager_list') }}</h3>
                </div>
            </div>
            <div class="k-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="k_table_1">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($records as $row)
                        <tr>
                            <td>{{ $row->first_name }}</td>
                            <td>{{ $row->email }}</td>
                            <td>
                                <form action="{{ route('managers.status', ['manager' => $row->id]) }}" method="POST">
                                    @csrf
                                    @method('put')
                                    <select name="status" id="status" class="form-control">
                                        <option value="1" {{ $row->active? 'selected': '' }}>Active</option>
                                        <option value="0" {{ !$row->active? 'selected': '' }}>Inactive</option>
                                    </select>
                                </form>
                                
                            </td>
                            <td>
                                <a href="{{ route('managers.edit', ['manager' => $row->id ]) }}">Edit</a>
                                <a href="{{ route('managers.destroy', ['manager' => $row->id ]) }}" onclick="event.preventDefault(); document.getElementById('destroy{{ $row->id }}').submit()">
                                    Delete
                                    <form action="{{ route('managers.destroy', ['manager' => $row->id ]) }}" id="destroy{{ $row->id }}" method="POST">
                                        @csrf
                                        @method('delete')
                                    </form>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>

@endsection

