@extends('admin.layouts.app')

@section('footer_scripts')
    <script src="{{ asset('assets/app\scripts\custom\dashboard.js') }}" type="text/javascript"></script>
@endsection

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.admin_dashboard') }}</h3>
@endsection

@section('breadcrumbs')

    {!! Breadcrumbs::render('home') !!}

@endsection

@section('content')

    <!--begin::Row-->
    <div class="row">
        <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
            <!--begin::Portlet-->
            <div class="k-portlet k-portlet--height-fluid k-widget-10">
                <div class="k-portlet__body">
                    <div class="k-widget-10__wrapper">
                        <div class="k-widget-10__details">
                            <div class="k-widget-10__title">0</div>
                        </div>
                        <div class="k-widget-10__num"></div>
                    </div>
                </div>
                <div class="k-portlet__body k-portlet__body--fit">
                    <div class="k-widget-10__chart">
                        <!--Doc: For the chart initialization refer to "mediumCharts" function in "src\theme\app\scripts\custom\dashboard.js" -->
                        <canvas id="k_widget_mini_chart_1" height="100"></canvas>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
        <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
            <!--begin::Portlet-->
            <div class="k-portlet k-portlet--height-fluid k-widget-10">
                <div class="k-portlet__body">
                    <div class="k-widget-10__wrapper">
                        <div class="k-widget-10__details">
                            <div class="k-widget-10__title">0</div>
                        </div>
                        <div class="k-widget-10__num">0</div>
                    </div>
                </div>
                <div class="k-portlet__body k-portlet__body--fit">
                    <div class="k-widget-10__chart">
                        <!--Doc: For the chart initialization refer to "mediumCharts" function in "src\theme\app\scripts\custom\dashboard.js" -->
                        <canvas id="k_widget_mini_chart_2" height="100"></canvas>
                    </div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>


@endsection

@section('footer-script')

    <script>

    </script>

@endsection