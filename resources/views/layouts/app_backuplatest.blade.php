<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
        window.user = {!!  json_encode(auth()->user())  !!};
    </script>
    <style>
        table td .dropdown-toggle:after {
            display: none;
        }

        table td .dropdown-toggle {
            background: none;
            border: none;
            font-size: 1.5em;
        }
        .rad-info-box {
            margin-bottom: 16px;
            box-shadow: 1px 1px 2px 0 #CCCCCC;
            padding: 20px;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
            background: white !important;
        }
        .rad-txt-success {
            color: #23AE89;
        }
        .rad-info-box i {
            display: block;
            background-clip: padding-box;
            margin-right: 15px;
            height: 60px;
            width: 60px;
            border-radius: 100%;
            line-height: 60px;
            text-align: center;
            font-size: 4.4em;
            position: absolute;
        }
        .rad-info-box .heading {
            font-size: 1.2em;
            font-weight: 300;
            text-transform: uppercase;
        }
        .rad-info-box .value, .rad-info-box .heading {
            display: block;
            position: relative;
            color: #515d6e;
            text-align: right;
            z-index: 10;
        }

        .rad-info-box .value {
            font-size: 2.1em;
            font-weight: 600;
            margin-top: 5px;
        }
        .rad-info-box .value, .rad-info-box .heading {
            display: block;
            position: relative;
            color: #515d6e;
            text-align: right;
            z-index: 10;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home') }}">
                    {{ config('app.name', 'Travel Groups') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Destinations <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('destinations.index') }}">Destinations List</a>
                                <a class="dropdown-item" href="{{ route('destinations.create') }}">Add Destination</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Groups <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('groups.index') }}">Groups List</a>
                                <a class="dropdown-item" href="{{ route('groups.create') }}">Add Group</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="{{ route('users.create') }}" class="nav-link">Register Customer</a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @if(session()->has('message'))
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if(session()->has('error'))
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @yield('content')
        </main>
    </div>
    @yield('footer-script')
</body>
</html>
