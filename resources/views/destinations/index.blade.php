@extends('layouts.app')

@section('footer_scripts')
    <!--begin::Page Vendors -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/datatables/basic/basic.js') }}" type="text/javascript"></script>
@endsection

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.destinations_list') }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('destinations') !!}
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('app.destinations_list') }}
                    <a href="{{ route('destinations.create') }}" class="btn btn-success float-right">{{ __('app.new_destinations') }}</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped- table-bordered table-hover table-checkable" id="k_table_1">
                    <thead>
                    <tr>
                        <th>{{ __('form.name') }}</th>
                        <th>{{ __('app.city') }}</th>
                        <th>{{ __('app.max_capacity') }}</th>
                        <th>{{ __('app.price') }}</th>
                        <th>{{ __('app.status') }}</th>
                        <th>{{ __('app.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($records as $row)
                        <tr>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->city }}</td>
                            <td>{{ $row->max_capacity }}</td>
                            <td>{{ $row->price }}</td>
                            <td>{{ $row->active }}</td>
                            <td>
                                <!-- <span class="dropdown show">
                                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                                      <i class="la la-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-218px, 25px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <a class="dropdown-item" href="#"><i class="la la-edit"></i> {{ __('app.edit')  }}</a>
                                        
                                    </div>
                                </span> -->
                                <a class="fa fa-edit btn btn-sm btn-outline-info" href="{{ route('destinations.edit', $row->id) }}"></a>
  
                                <a class="fa fa-trash btn btn-sm btn-outline-danger" href="#" onclick="event.preventDefault(); if(confirm('Do you want to delete?')){ document.getElementById('row{{ $row->id }}').submit(); }">
                                    <form action="{{ route('destinations.destroy', $row->id) }}" id="row{{ $row->id }}" class="d-inline" method="post">
                                        @csrf
                                        @method('delete')
                                    </form>
                                    <!-- {{ __('app.delete')  }} -->
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
