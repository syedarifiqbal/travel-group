@extends('layouts.app')

@section('page_title')
    <h3 class="k-content__head-title">{{ __('app.new_destinations') }}</h3>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('destinations.create', $destination->id) !!}
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('app.new_destinations') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" class="k-form" action="{{ $destination->id? route('destinations.update', ['destination' => $destination->id]): route('destinations.store') }}">
                    @csrf
                    @if($destination->id) @method('put') @endif

                    <div class="k-portlet__body">

                        <div class="form-group validated">
                            <label for="name">{{ __('form.name') }}</label>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $destination->name) }}" required autofocus>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group validated">
                            <label for="city">{{ __('form.city') }}</label>
                            <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city', $destination->city) }}" required>
                            @if ($errors->has('city'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('city') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group validated">
                            <label for="max_capacity">{{ __('form.max_capacity') }}</label>
                            <input id="max_capacity" type="text" class="form-control{{ $errors->has('max_capacity') ? ' is-invalid' : '' }}" name="max_capacity" value="{{ old('max_capacity', $destination->max_capacity) }}" required>
                            @if ($errors->has('max_capacity'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('max_capacity') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group validated">
                            <label for="price">{{ __('form.price') }}</label>
                            <input id="price" type="number" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" value="{{ old('price', $destination->price) }}" required>
                            @if ($errors->has('price'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="k-portlet__foot">
                        <div class="k-form__actions k-form__actions">
                            <button type="submit" class="btn btn-brand">{{ __('form.submit') }}</button>
                            <a href="{{ route('destinations.index') }}" class="btn btn-secondary">{{ __('form.cancel') }}</a>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- <div class="row">
        <div class="col-lg-12">
            <div class="k-portlet">
                <div class="k-portlet__head">
                    <div class="k-portlet__head-label">
                        <h3 class="k-portlet__head-title"></h3>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

@endsection
