require('./bootstrap');

import CircleChart from 'circle-chart';

window.CircleChart = CircleChart;

import { Form, HasError, AlertError, AlertSuccess } from 'vform';

window.Form = Form;
window.HasError = HasError;
window.AlertError = AlertError;
window.AlertSuccess = AlertSuccess;

window.Vue = require('vue');

import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);

import vSelect from 'vue-select';
Vue.component('v-select', vSelect);
Vue.component('group-payment-form', require('./components/group-payment-form'));
Vue.component('create-group', require('./components/create-group.vue'));

Vue.component('example-component', require('./components/ExampleComponent'));
Vue.component('add-flight', require('./components/AddFlightComponent'));
Vue.component('group-customer-list', require('./components/CustomerGroupListComponent'));

const app = new Vue({
    el: '#app'
});

require('./custom');