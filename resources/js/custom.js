import CircleChart from "circle-chart";
import Axios from "axios";

let paidEl = document.getElementById('paid');
let remainEl = document.getElementById('remain');

$(function(){
    if (paidEl)
    {
        new CircleChart({
            $container: paidEl,
            ringProportion: 0.35,
            tooltips: true,
            staticTotal: true,
            total: 100,
            middleCircleColor: '#f8f8f8',
            background: '#eee',
            definition: [
                {color: '#007bff', value: paidEl.dataset.amount}
            ]
        });
    }
    if (remainEl)
    {
        new CircleChart({
            $container: remainEl,
            ringProportion: 0.35,
            tooltips: true,
            staticTotal: true,
            total: 100,
            middleCircleColor: '#f8f8f8',
            background: '#eee',
            definition: [
                {color: '#007bff', value: remainEl.dataset.amount}
            ]
        });
    }
});


$(function(){
    $('select[name=status]').on('change', function(e){
        if(confirm('Are you sure you want to deactivate this manager?'))
        {
            axios.put($(this).parents('form').attr('action'))
            .then(data => {
                console.log(data);
            });
        }
    });
});