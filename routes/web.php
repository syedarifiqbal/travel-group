<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('admin_or_manager:admin,manager')->group(function(){

    Route::get('seed', 'SeedsController@index');
    Route::resource('destinations', 'DestinationController');
    Route::resource('groups', 'GroupController');
    Route::get('customers', 'UserController@customer');
    Route::get('groups/{group}/payments/create', 'PaymentController@groupPaymentFrom')->name('groups.payments.create');
    Route::post('groups/{group}/payments', 'PaymentController@store')->name('groups.payments.store');
    Route::get('groups/{group}/payments/{payment}/edit', 'PaymentController@edit')->name('groups.payment.edit');
    Route::get('groups/{group}/payments/{payment}', 'PaymentController@show')->name('groups.payment.show');
    Route::post('groups/{group}/payments/{payment}', 'PaymentController@update')->name('groups.payment.update');
    Route::get('groups/{group}/customers/json', 'GroupController@customerJson')->name('groups.customers.json');

    Route::get('payments/{payment}/download', 'PaymentController@downloadProof')->name('payments.proof.download');

    Route::get('customers', 'UserController@index')->name('customers.index');
    Route::get('customers/json', 'UserController@userJson')->name('customers.json');
    Route::get('customers/create', 'UserController@create')->name('customers.create');
    Route::post('customers', 'UserController@store')->name('customers.store');
    Route::post('customer-balance', 'PaymentController@customerBalance')->name('customers.balance');
    Route::post('get-customer-flight-number', 'FlightController@getCustomerFlight')->name('customers.flight');
    Route::get('groups/{group}/customers/show', 'CustomerGroupController@getCustomers')->name('groups.customers');
    Route::post('groups/{group}/customer/flight', 'CustomerGroupController@storeCustomerFlight')->name('groups.customers.flight.store');

});


Route::group(['prefix' => 'admin'],function () {
    Route::get('register', 'AdminController@create')->name('admin.register');
    Route::post('register', 'AdminController@store')->name('admin.register.store');
    Route::get('login', 'Auth\Admin\LoginController@login')->name('admin.auth.login');
    Route::post('login', 'Auth\Admin\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
    Route::post('logout', 'Auth\Admin\LoginController@logout')->name('admin.auth.logout');
});

Route::group(['prefix' => 'admin','middleware' => 'admin'],function () {
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('dashboard', 'AdminController@index')->name('admin.dashboard');
    Route::get('managers/create', 'ManagerController@create')->name('managers.create');
    Route::get('managers', 'ManagerController@index')->name('managers.index');
    Route::post('managers', 'ManagerController@managerStore')->name('managers.store');
    Route::get('managers/{manager}/edit', 'ManagerController@edit')->name('managers.edit');
    Route::put('managers/{manager}/status', 'ManagerController@status')->name('managers.status');
    Route::put('managers/{manager}', 'ManagerController@update')->name('managers.update');
    Route::delete('managers/{manager}', 'ManagerController@destroy')->name('managers.destroy');
});

Route::prefix('manager')->group(function () {
    Route::get('/', 'ManagerController@index')->name('manager.dashboard');
    Route::get('dashboard', 'ManagerController@index')->name('manager.dashboard');
    Route::get('register', 'ManagerController@create')->name('manager.register');
    Route::post('register', 'ManagerController@store')->name('manager.register.store');
    Route::get('login', 'Auth\Manager\LoginController@login')->name('manager.auth.login');
    Route::post('login', 'Auth\Manager\LoginController@loginAdmin')->name('manager.auth.loginManager');
    Route::post('logout', 'Auth\Manager\LoginController@logout')->name('manager.auth.logout');
});



Route::get('/clear-cache', function(){
    $re = \Artisan::call('cache:clear');
    $re = \Artisan::call('config:cache');
    return $re;
});

Route::get('/migrate', function(){
    $re = \Artisan::call('migrate');
    return $re;
});