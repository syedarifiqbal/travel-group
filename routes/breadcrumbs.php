<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('<i class="flaticon2-shelter"></i>', route('home'));
});

// Home > Destinations
Breadcrumbs::for('destinations', function ($trail) {
    $trail->parent('home');
    $trail->push(__('app.destinations'), route('destinations.index'));
});

// Home > Destinations > New/Edit
Breadcrumbs::for('destinations.create', function ($trail, $destination) {
    $trail->parent('destinations');
    $trail->push($destination? __('app.edit'): __('app.new'), route('destinations.index'));
});

// Home > Groups
Breadcrumbs::for('groups', function ($trail) {
    $trail->parent('home');
    $trail->push(__('app.groups'), route('groups.index'));
});

// Home > Destinations > New/Edit
Breadcrumbs::for('groups.create', function ($trail, $destination) {
    $trail->parent('groups');
    $trail->push($destination? __('app.edit'): __('app.new'), route('groups.index'));
});

// Home > Destinations > Destination name
Breadcrumbs::for('groups.show', function ($trail, $group) {
    $trail->parent('groups');
    $trail->push($group->destination->name, route('groups.show', [$group->id]));
});

// Home > Destinations > Destination name
Breadcrumbs::for('groups.payment', function ($trail, $group) {
    $trail->parent('groups');
    $trail->push($group->destination->name, route('groups.show', [$group->id]));
    $trail->push('Payment');
});

// Home > Customers
Breadcrumbs::for('customers', function ($trail) {
    $trail->parent('home');
    $trail->push(__('app.customers'), route('customers.index'));
});

// Home > Customers > New/Edit
Breadcrumbs::for('users.create', function ($trail, $user) {
    $trail->parent('customers');
    $trail->push($user? __('app.edit'): __('app.new'), route('customers.index'));
});

// Home > Admin
Breadcrumbs::for('admin', function ($trail) {
    $trail->parent('home');
    $trail->push(__('app.admin'), route('admin.dashboard'));
});

// Home > Admin > Managers > Create
Breadcrumbs::for('managers', function ($trail) {
    $trail->parent('admin');
    $trail->push(__('app.manager'), route('managers.index'));
});

// Home > Admin > Managers > Create
Breadcrumbs::for('managers.create', function ($trail) {
    $trail->parent('managers');
    $trail->push(__('app.create_manager'), route('managers.create'));
});

// Home > About
//Breadcrumbs::for('about', function ($trail) {
//    $trail->parent('home');
//    $trail->push('About', route('about'));
//});